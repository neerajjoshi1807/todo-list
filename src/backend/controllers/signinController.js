const db = require("../databases/sqlite");
const lists = db.lists;
const users = db.users;

const signinpage = (req, res) => {
    //let pathtohtml= path.join(__dirname,"../../client/views/signin.ejs");
    return res.render("signin");
};

const signinuser = (req, res) => {
  const { email, password } = req.body;
  if(!(email && password))
  {
    return res.redirect('/signin')
  }
  else
  {
  users
    .findOne({
      where: {
        email: email,
        password: password
      }
    })
    .then(r => {
      if (r) {
        console.log(r);
        const email=r.email;
        const name=r.name;

        req.session.user = { email, name };
        return res.redirect("/");
      } else {
        return res.redirect("/signin");
      }
    })
    .catch(err => {
      console.log(err);
      return res.redirect("/signin");
    });
};
}

const signout = (req, res) => {
  req.session.destroy(() => {
    console.log("err occured :( ");
  });
  return res.redirect("/signin");
};
  
module.exports = {
     signinpage:signinpage,
     signinuser:signinuser,
     signout:signout
};

